# Apache Camel



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/taskar.sushil/apache-camel.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/taskar.sushil/apache-camel/-/settings/integrations)

Please follow link for complete understanding - 
1. https://medium.com/@sushil240289/apache-camel-4-x-spring-boot-3-x-rabbitmq-ibmmq-628015626dfe
2. https://dev.to/sushil/apache-camel-4x-spring-boot-3x-rabbitmq-ibmmq-3293